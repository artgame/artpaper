# ArtPaper

The main goal of this fork is to remove unneeded features for our creative plot server.

## Table of contents
- [Build](#build)
- [Run](#run)
- [Upstream](#upstream)
- [Updating](#updating)
- [Tasks](#tasks)

## Build
1. `gradlew applyPatches`
2. `gradlew createMojmapPaperclipJar`

## Run
```shell
./gradlew runDev
```

## Upstream
1. Edit the `gradle.properties` file with new commit hash
2. Run commands
```shell
./gradlew applyPatches
./gradlew rebuildPatches
```
3. Commit your changes.

## Updating
1. Checkout to latest version
2. Run commands below
```shell
git checkout -b ver/NEW_VERSION
git remote add paper https://github.com/PaperMC/paperweight-examples
git fetch paper
```
3. Then cherry-pick new changes from paper repo
```shell
git cherry-pick 8a2403ad70bf532554b0d5b4f9d0abbf523c28f5
# solve conflicts if any
# vscode is recommended
git add .
git cherry-pick --continue
# vim will open with commit message, save and exit
# :wq
```
4. Remove all old patches if the update is major (ex 1.20 -> 1.21)
```shell
rm -rf patches
```
5. Execute `gradlew applyPatches` to start with empty project

See [CONTRIBUTING.md](https://github.com/PaperMC/Paper/blob/master/CONTRIBUTING.md) for more information.

## Tasks
```
Paperweight tasks
-----------------
applyApiPatches
applyPatches
applyServerPatches
cleanCache - Delete the project setup cache and task outputs.
createMojmapBundlerJar - Build a runnable bundler jar
createMojmapPaperclipJar - Build a runnable paperclip jar
createReobfBundlerJar - Build a runnable bundler jar
createReobfPaperclipJar - Build a runnable paperclip jar
generateDevelopmentBundle
rebuildApiPatches
rebuildPatches
rebuildServerPatches
reobfJar - Re-obfuscate the built jar to obf mappings
runDev - Spin up a non-relocated Mojang-mapped test server
runReobf - Spin up a test server from the reobfJar output jar
runShadow - Spin up a test server from the shadowJar archiveFile
```